<?php // concordance.php
/**
 * Position:   PHP Software Developer (1129.18C.300), Arvada
 * Company:    Untangle, Inc.
 *
 * Given an arbitrary text document written in English, write a program that will generate a concordance
 *
 * Bonus: label each word with the sentence numbers in which each occurrence appeared.
 */
error_reporting(E_ALL);

// A CLASS TO IDENTIFY THE TERMS AND COMPONENT WORDS
Class Concordance
{
    /**
     * This is the return array of words and counts
     */
    protected $word_counts = [];

    /**
     * This is the list of words that should be excluded from the lookup.  Mostly these
     * are common words without "term of art" meanings.  You might want to add to this
     * list to suppress false-positive matches in your industry.  If you're a church
     * you might want to exclude "bible" or "worship"; if you're a car dealership you
     * might want to exclude "car" from the search, etc.
     */
    protected $exclusions = array
    ( 'the'
    , 'and'
    , 'to'
    , 'of'
    , 'a'
    , 'in'
    , 'was'
    , 'he'
    , 'that'
    , 'it'
    , 'his'
    , 'her'
    , 'you'
    , 'as'
    , 'with'
    , 'for'
    , 'is'
    , 'or'
    , 'an'
    , 'this'
    , 'like'
    , 'I'
    , 'at', 'on', 'we', 'if', 'so', 'our'
    , 'said', 'are', 'had'
    , 'were', 'has', 'did'
    , 'nbsp', 'quot', 'hellip'
    )
    ;


    /**
     * This is the list of words that should be included from the lookup, even when
     * normalization would probably remove them from the results.  These do not
     * actually have to be words.  They are case-sensitive.
     */
    protected $inclusions = array
    ( 'C-3PO'
    , 'R2-D2'
    )
    ;

    /**
     * This is the list of word suffix that should be excluded
     */
    protected $suffix_rejects = array
    ( "'s"
    , "'re"
    , "n't"
    )
    ;

    public function addExclusions(array $words)
    {
        $this->exclusions = array_merge($words, $this->exclusions);
    }

    public function addInclusions(array $words)
    {
        $this->inclusions = array_merge($words, $this->inclusions);
    }

    public function sortOnAlphabetical(array $words)
    {
        return natsort($words);
    }

    public function getFrequencies($text)
    {
        // INCLUSIONS CASE-SENSITIVE
        $words = [];
        foreach ($this->inclusions as $word)
        {
            $cntr = preg_match_all('/\b' . preg_quote($word) . '\b/', $text, $mat);
            if ($cntr) $words[$word] = $cntr;
            $text = preg_replace('/\b' . preg_quote($word) . '\b/', ' ', $text);
        }

        // CASE-INSENSITIVE
        $text = strtolower($text);

        // NORMALIZATION
        $text = preg_replace('/[^\'a-z-_\s]/i', ' ', $text);
        // var_dump($text);

        // SUFFIX-REJECTS CASE-INSENSITIVE
        $excls = [];
        foreach ($this->suffix_rejects as $key => $word)
        {
            $excls[$key] ='/' . preg_quote($word) . '\b/i';
        }
        $text = preg_replace($excls, ' ', $text);

        // WORD COUNTS
        $text = preg_replace('/\s+/', ' ', $text);
        $arry = explode(' ', $text);
        $arry = array_count_values($arry);

        // EXCLUSIONS CASE-INSENSITIVE
        foreach ($this->exclusions as $key => $word) $this->exclusions[$key] = strtolower($word);
        foreach ($arry as $key => $cnt)
        {
            if (in_array($key, $this->exclusions)) unset($arry[$key]);
        }

        $this->word_counts = array_merge($words + $arry);

        asort($this->word_counts);
        return $this->word_counts;
    }
}

$str = $file = file_get_contents('./about_untangle.txt', FILE_USE_INCLUDE_PATH);

$wf = new Concordance;

$arr = $wf->getFrequencies($str);
print_r($arr);
